import java.net.*;
import java.io.*;
import java.util.ArrayList;
import javax.xml.bind.DatatypeConverter;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MultiServerThread extends Thread {
   private Socket socket = null;

   public MultiServerThread(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      ServerMultiClient.NoClients++;
   }

   public void run() {

      try {
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn;
		
	    while((lineIn = entrada.readLine()) != null){
            System.out.println("Received: "+lineIn);
            escritor.flush();
            if(lineIn.equals("FIN")){
               ServerMultiClient.NoClients--;
                break;
            }else if(lineIn.equals("#clientes#")){//Cantidad de clientes conectados
               escritor.println("Clientes conectados: "+ServerMultiClient.NoClients);
               escritor.flush();
            }else if(lineIn.charAt(0)=='#' && lineIn.charAt(1)=='1' && lineIn.charAt(2)=='|'){//Encripta una cadena
                //CADENA EJEMPLO PARA USAR EL ENCRIPTADO: #1|Hola
                String subLine = lineIn.substring(3, lineIn.length());
                String cadenaEncriptada = encriptar(subLine);

                escritor.println("CADENA ENCRIPTADA: "+cadenaEncriptada);
                escritor.flush();  
            }else if(lineIn.charAt(0)=='#' && lineIn.charAt(1)=='2' && lineIn.charAt(2)=='|'){//Desencripta una cadena
                //CADENA EJEMPLO PARA USAR EL DESENCRIPTADO: #2|SG9sYQ==
                String subLine = lineIn.substring(3, lineIn.length());
                String cadenaDesencriptada = desencriptar(subLine);

                escritor.println("CADENA DESENCRIPTADA: "+cadenaDesencriptada);
                escritor.flush();
            }else if(lineIn.charAt(0)=='#'){
                System.out.println("Entramos al servicio");
                if(lineIn.charAt(lineIn.length()-1)=='&'){
                    System.out.println("Entramos al if para checar sintaxis");
                    if(lineIn.charAt(1)=='6' && lineIn.charAt(2)=='2' && lineIn.charAt(3)=='2'&& lineIn.charAt(4)=='|'){
                        System.out.println("Entramos al if del codigo");
                        String subLine = lineIn.substring(5, lineIn.length()-1);
                        escritor.println(" "+ subLine);
                        escritor.flush();
                         String  n,sn ="";
        int x=0;
        for (int i=0; i<subLine.length(); i++){
        x=subLine.charAt(i);
        n=Integer.toBinaryString(x);
        sn= sn+n;
        System.out.println(subLine.charAt(i));
        System.out.println(x);
        System.out.println(n+"\n");
        }
        escritor.println(" "+sn);
        escritor.flush();
                        
                    }else{
                        escritor.println("ERROR! ->codigo incorrecto<-");
                        escritor.flush();
                    }
                }else{
                    escritor.println("ERROR! ->sintaxis incorrecta<-");
                    escritor.flush();
                }
            }else{
                escritor.println("Echo... "+lineIn);
                escritor.flush();
            }
         } 
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(IOException e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      } catch (Exception ex) {
           Logger.getLogger(MultiServerThread.class.getName()).log(Level.SEVERE, null, ex);
       }
   }
   
    public static String toHexadecimal(String text) throws UnsupportedEncodingException{
        byte[] myBytes = text.getBytes("UTF-8");
        return DatatypeConverter.printHexBinary(myBytes);
    }
    
    public static String convertArrayToString(String[] strArray) {
        String joinedString = String.join("|", strArray);
        return joinedString;
    }
    
    private static String encriptar(String s) throws UnsupportedEncodingException{
        return Base64.getEncoder().encodeToString(s.getBytes("utf-8"));
    }
    
    private static String desencriptar(String s) throws UnsupportedEncodingException{
        byte[] decode = Base64.getDecoder().decode(s.getBytes());
        return new String(decode, "utf-8");
    }
   
} 

